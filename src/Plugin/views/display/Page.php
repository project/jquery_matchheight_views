<?php

namespace Drupal\jquery_matchheight_views\Plugin\views\display;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\Page as CorePage;

/**
 * The plugin that handles a full page.
 *
 * @ingroup views_display_plugins
 *
 * @ViewsDisplay(
 *   id = "page",
 *   title = @Translation("Page"),
 *   help = @Translation("Display the view as a page, with a URL and menu links."),
 *   uses_menu_links = TRUE,
 *   uses_route = TRUE,
 *   contextual_links_locations = {"page"},
 *   theme = "views_view",
 *   admin = @Translation("Page")
 * )
 */
class Page extends CorePage {

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);
    $jquery_matchHeight = $this->getOption('jquery_matchHeight');

    $options['jquery_matchHeight'] = [
      'category' => 'other',
      'title' => $this->t('jquery.matchHeight'),
      'value' =>  $jquery_matchHeight['enabled'] ? $this->t('Yes') : $this->t('No'),
      'desc' => $this->t('Config options for jquery.matchHeight'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['jquery_matchHeight'] = [
      'contains' => [
        'enabled' => ['default' => FALSE],
        'settings' => ['default' => ''],
      ]
    ];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    if ($form_state->get('section') == 'jquery_matchHeight') {
      $jquery_matchHeight = $this->getOption('jquery_matchHeight');
      $form['#title'] .= $this->t('jquery.matchHeight');
      $form['jquery_matchHeight'] = [
        '#prefix' => '<div class="clearfix">',
        '#suffix' => '</div>',
        '#tree' => TRUE,
      ];
      $form['jquery_matchHeight']['enabled'] = [
        '#prefix' => '<div class="views-left-30">',
        '#suffix' => '</div>',
        '#title' => $this->t('Enabled'),
        '#type' => 'checkbox',
        '#default_value' => $jquery_matchHeight['enabled'],
      ];
      $desc = "Format: <strong>[selector]|[byRow]|[property]|[target]|[remove]</strong></br>";
      $desc .= "<span>&emsp;</span>[selector]  is a syntax used in jQuery to select HTML elements based on various criteria such as element type, class, ID, attribute, etc. It allows you to easily manipulate and interact with selected elements on a web page.</br>";
      $desc .= "The default options contain: </br>";
      $desc .= "<span>&emsp;</span>[byRow]: true, byRow is true or false to enable row detection</br>";
      $desc .= "<span>&emsp;</span>[property]: property, is the CSS property name to set (e.g. 'height' or 'min-height')'height'</br>";
      $desc .= "<span>&emsp;</span>[target]: null,target is an optional element to use instead of the element with maximum height</br>";
      $desc .= "<span>&emsp;</span>[remove]: false,remove is true or false to remove previous bindings instead of applying new ones</br>";
      $desc .= "If you do not want to set a value for the option, please use <strong>null</strong>.</br>";
      $desc .= "Each settings value will be on a row.</br>";
      $desc .= "Example:</br>";
      $desc .= "<span>&emsp;</span><strong>.views-field-body|false|min-height|null|false</strong></br>";
      $desc .= "<span>&emsp;</span><strong>.views-field-field-image|false|min-height|null|false</strong></br>";

      $form['jquery_matchHeight']['settings'] = [
        '#title' => $this->t('Settings'),
        '#type' => 'textarea',
        '#description' => $desc,
        '#default_value' => $jquery_matchHeight['settings'],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);
    if ($form_state->get('section') == 'jquery_matchHeight') {
      $this->setOption('jquery_matchHeight', $form_state->getValue('jquery_matchHeight'));
    }
  }
}
