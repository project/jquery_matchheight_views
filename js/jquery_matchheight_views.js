(function ($, Drupal, drupalSettings) {
  'use strict';

  Drupal.jquery_matchHeight = Drupal.jquery_matchHeight || {};
  Drupal.jquery_matchHeight.once = true;

  Drupal.behaviors.jquery_matchHeight = {
    attach: function (context, settings) {
      $(document).ready(function () {
        if (Drupal.jquery_matchHeight.once && drupalSettings.jquery_matchHeight) {
          let data = drupalSettings.jquery_matchHeight;
          for (const key in data) {
            if (data.hasOwnProperty(key)) {
              const values = data[key];
              if (values) {
                values.forEach(function (item) {
                  if (item.selector && item.options) {
                    $(item.selector).matchHeight(item.options);
                  }
                });
              }
            }
          }
          Drupal.jquery_matchHeight.once = false;
        }
      });
    }
  }
})(jQuery, Drupal, drupalSettings);
