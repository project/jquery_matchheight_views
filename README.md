# jquery Match Height views

Allowed config js match height in views

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires the following modules:

- views

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

This module has the config in the views page and views block

## Supporting organizations:

- [hungtrv99](https://www.drupal.org/u/hungtrv99)
